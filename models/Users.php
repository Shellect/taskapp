<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 12.07.20
 * Time: 1:57
 */
namespace models;

use \core\Model;

class Users extends Model{
	protected $id;
	public $username;
	public $pass;

	public function fieldsTable() {
		return array(
			'username' => 'Имя пользователя',
			'pass' => 'Пароль',
		);
	}
}