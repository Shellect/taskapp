<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 10.07.20
 * Time: 22:57
 */

require_once __DIR__ . '/core/DB.php';

use core\DB;

$pdo = DB::connect();

/**
 * Получаем файлы миграций
 * @param $pdo
 *
 * @return array
 */
function getMigrationFiles( $pdo ) {
	$dir      = __DIR__ . '/migrations/';
	$allFiles = glob( $dir . '*.sql' );

	$query = $pdo->query("SHOW TABLES LIKE 'versions';");
	$data = $query->fetchAll();

	if ( !count($data) ) {
		return $allFiles;
	}
	$versionsFiles = array();
	$data  = $pdo->query( 'SELECT `name` FROM `versions`;' );
	foreach ( $data as $row ) {
		array_push( $versionsFiles, $dir.$row['name'] );
	}

	return array_diff( $allFiles, $versionsFiles );
}

/**
 * Применяем миграцию
 * @param $pdo
 * @param $file
 */
function migrate( $pdo, $file ) {
	$command = file_get_contents($file);
	$pdo->exec($command);

	$baseName = basename($file);
	$query = $pdo->prepare('INSERT INTO `versions` (`name`) VALUES(:basename)');
	$query->bindParam(':basename',$baseName, PDO::PARAM_STR);
	$query->execute();
}

$files = getMigrationFiles( $pdo );

if ( empty( $files ) ) {
	echo 'База в актуальном состоянии'.PHP_EOL;
} else {
	foreach ( $files as $file ) {
		migrate( $pdo, $file );
	}
	echo 'Миграции успешно применены'.PHP_EOL;
}
