<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 14.07.20
 * Time: 12:38
 */

namespace controllers;

use \core\Controller;
use \models\Users;

class Controller_Login extends Controller {
	public function action_index( $params = [] ) {
		$auth = $_SESSION['auth']??false;
		if ( ! empty( $_POST ) ) {
			$login = $_POST['username'] ?? false;
			$pass  = $_POST['pass'] ?? false;
			$select = array(
				'where' => "username='{$login}'"
			);
			$model  = new Users( $select );
			$keys     = $model->fieldsTable();
			$response = [];
			foreach ( $keys as $key => $value ) {

				if ( $_POST[ $key ] === '' ) {
					$response[ $key ] = "Необходимо заполнить поле {$value}";
					continue;
				}
			}
			if ( empty( $response ) ) {

				if ($model->fetchOne() && password_verify( $pass, $model->pass )){
					$_SESSION['auth'] = true;
					header("Location: /");
					return true;
				}else{
					$response['status'] = 'Логин или пароль введен неверно.';
				}
			}
				$this->view->generate( 'login.php', 'layout.php', ['response'=>$response] );


		} else if ($auth){
			header("Location: /");
			return true;
		} else {
			$this->view->generate( 'login.php', 'layout.php' );
		}
	}

	public function action_logout(){
		$_SESSION['auth'] = false;
		session_destroy();
		header("Location: /");
		return true;
	}
}