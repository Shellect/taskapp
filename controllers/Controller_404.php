<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 12.07.20
 * Time: 22:11
 */

namespace controllers;

use \core\Controller;

/**
 * Class Controller_404
 * @package controllers
 */
class Controller_404 extends Controller {
	public function action_index($params=[]) {
		$this->view->generate('404.php', 'layout.php');
	}
}