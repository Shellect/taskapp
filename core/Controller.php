<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 12.07.20
 * Time: 21:39
 */

namespace core;


abstract class Controller {
	public $model;
	public $view;

	public function __construct()
	{
		$this->view = new View();
	}

	abstract public function action_index($params = []);
}