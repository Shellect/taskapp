$(document)
    .on('click', '.task-completed', event => {
        "use strict";

        let checkbox = $(event.target).parents('.task-completed');
        let data = new FormData();
        let dataID = checkbox.data('id');
        let dataCompleted = checkbox.data('completed');
        data.append('id', dataID);
        data.append('completed', dataCompleted);
        $.ajax({
            url: '/task/check',
            method: 'POST',
            contentType: false,
            processData: false,
            data: data,
            success: function (response) {
                let resp = JSON.parse(response);
                if (resp['status']) {
                    checkbox.data('completed', resp['completed']);
                    let img = new Image();
                    img.src = resp['completed']
                        ? '/assets/img/checkbox-checked.svg'
                        : '/assets/img/checkbox-unchecked.svg';
                    img.onload = () => {
                        checkbox.html(img);
                    };
                }else {
                    checkbox.attr('data-toggle', 'tooltip');
                    checkbox.attr('data-placement', 'top');
                    checkbox.attr('data-title', 'Ошибка сохранения');
                    $(checkbox).tooltip('show');
                }
            },
            error: function (error) {
                console.error(error);
            }
        });

    })
    .on('click', '.task-text', event => {
        "use strict";
        if (event.target.tagName === 'DIV') {

            $('textarea').each((index, el)=>{
                blurTextArea(el);
            });


            let text = $(event.target);
            let dataText = text.text();
            text.html(`
<div class="row align-items-center">
    <div class="col-sm-10">
        <textarea rows="5" class="form-control">${dataText}</textarea>
    </div>
    <div class="col-sm-2">
        <button class="task-save btn btn-primary" type="button">Сохранить</button>
    </div>
</div>
`);
        }
    })
    .on('click', '.task-save', event => {
        "use strict";

        let text = $(event.target).parents('.task-text');
        let textArea = text.find('textarea');
        let data = new FormData();
        let dataID = text.data('id');
        let dataText = textArea.val();
        data.append('id', dataID);
        data.append('text', dataText);
        $.ajax({
            url: '/task/edit',
            method: 'POST',
            contentType: false,
            processData: false,
            data: data,
            success: function (response) {
                let resp = JSON.parse(response);
                if (resp['status']) {
                    window.location.reload();
                }else {
                    $(event.target).attr('data-toggle', 'tooltip');
                    $(event.target).attr('data-placement', 'top');
                    $(event.target).attr('data-title', 'Ошибка сохранения');
                    $(event.target).tooltip('show');
                }
            },
            error: function (error) {
                console.error(error);
            }
        });

    });


function blurTextArea(textArea) {
    let text = $(textArea).parents('.task-text');
    let dataText = textArea.value;
    text.html(dataText);
}