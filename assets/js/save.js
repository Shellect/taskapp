'use strict';

$('#saveButton').click(function(event){
    event.preventDefault();
    $('.task-info').html('');
    let form = document.forms.saveForm;
    let data = new FormData(form);
    data.append('completed', 0);
    $.ajax({
        url: '/task/save',
        method: 'POST',
        contentType:false,
        processData:false,
        data: data,
        success: function(response){
            let resp = JSON.parse(response);
            if (resp['status']){
                $('#successModal').modal('show');
            }else{
                validationErrors(resp);
            }
        },
        error:function(error){
            console.error(error);
        }
    });
});

function validationErrors(log){
    Object.keys(log).forEach(el=>{
        let span = document.getElementById(el);
        if (span) span.innerText = log[el];
    });
}