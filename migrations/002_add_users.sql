-- Таблица users --
CREATE TABLE `users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(255) NOT NULL UNIQUE ,
  `pass` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (id)
)
  CHARACTER SET utf8mb4
  COLLATE utf8mb4_general_ci;

INSERT INTO `users` (username,pass)
  VALUES ('admin','$2y$10$rCUy4CJ21B1MW.u7.l1sJemMI9.DjppvA7FWiKFm6CeAki4jYH9GO');